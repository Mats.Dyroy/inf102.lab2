package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) {
		if (index < 0 || index >= n) throw new IndexOutOfBoundsException("Index is outside the array.");
		return (T) elements[index];
	}
	
	@Override
	public void add(int index, T element) {

		// Ensure internal array has room for new element
		var requiredCapacity = Math.max(index, n) + 1;
		ensureHasCapacity(requiredCapacity);

		// Move preceding elements down the list
		for (var i = n; i > index; i--)
			elements[i] = elements[i - 1];

		// Insert value
		elements[index] = element;

		n = requiredCapacity;
	}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

	private void ensureHasCapacity(int count) {
		// Stop if there is no need to increase capacity.
		if (count <= elements.length) return;

		// Allocate new array
		var newElements = new Object[count * 2];

		// Copy over existing elements
		for (var i = 0; i < elements.length; i++)
			newElements[i] = elements[i];

		// Replace array pointer
		elements = newElements;
	}
}
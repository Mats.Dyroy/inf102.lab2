package INF102.lab2.list;

public class LinkedList<T> implements List<T> {

	private int n;
	
	/**
	 * If list is empty, head == null
	 * else head is the first element of the list.
	 */
	private Node<T> head;

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}

	@Override
	public T get(int index) {
		return getNode(index).data;
	}
	
	/**
     * Returns the node at the specified position in this list.
     *
     * @param index index of the node to return
     * @return the node at the specified position in this list
     * @throws IndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
     */
	private Node<T> getNode(int index) {
		if (index < 0 || index >= n) throw new IndexOutOfBoundsException();

		var node = head;
		for (var i = 0; i < index; i++)
			node = node.next;
		return node;
	}

	@Override
	public void add(int index, T element) {
		if (index < 0) throw new IndexOutOfBoundsException();

		var newNode = new Node<T>(element);

		// Prepend
		if (index == 0) {
			newNode.next = head;
			head = newNode;
			n++;
			return;
		}

		// Insert
		if (index < n) {
			var previous = getNode(index - 1);
			newNode.next = previous.next;
			previous.next = newNode;
			n++;
			return;
		}

		// Append
		var diff = index - n + 1;
		var node = n > 0 ? getNode(n - 1) : (head = new Node<T>(null));
		// Pad end of list if inserted index is greater than n.
		for (var i = 0; i < diff; i++) {
			var nullNode = new Node<T>(null);
			node.next = nullNode;
			node = nullNode;
		}
		node.data = element;
		n += diff;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		Node<T> currentNode = head;
		while (currentNode.next != null) {
			str.append(currentNode.data);
			str.append(", ");
			currentNode = currentNode.next;
		}
		str.append((T) currentNode.data);
		str.append("]");
		return str.toString();
	}

	@SuppressWarnings("hiding")
	private class Node<T> {
		T data;
		Node<T> next;

		public Node(T data) {
			this.data = data;
		}
	}
	
}